package ru.tinkoff.simpleapp

import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.softwaremill.macwire.wireSet
import ru.tinkoff.simpleapp.common.RuntimeModule
import ru.tinkoff.simpleapp.common.api.ApiModule
import ru.tinkoff.simpleapp.common.config.ConfigModule
import ru.tinkoff.simpleapp.common.db.DatabaseModule
import ru.tinkoff.simpleapp.payment.PaymentModule

import scala.annotation.unused
import scala.concurrent.Future

class AppModule(@unused paymentModule: PaymentModule,
                runtimeModule: RuntimeModule,
                databaseModule: DatabaseModule,
                configModule: ConfigModule) {

  import runtimeModule._
  import configModule.config

  private val apiModules: Seq[ApiModule] = wireSet[ApiModule].toSeq

  private val route: Route =apiModules.flatMap(_.controllers).map(_.route).reduce(_ ~ _)

  // just for testing purpose
  def initSchema(): Future[Unit] = {
    import databaseModule.db
    import db.profile.api._

    db.run(paymentModule.paymentDB.AllPayments.schema.create)
  }

  def run(): Future[Unit] = {
    Http().bindAndHandle(route, "localhost", config.getInt("api.port"))
      .map(_ => ())
  }
}
