package ru.tinkoff.simpleapp

import com.softwaremill.macwire._
import com.typesafe.scalalogging.Logger
import ru.tinkoff.simpleapp.common.DefaultRuntimeModule
import ru.tinkoff.simpleapp.common.config.ConfigModule
import ru.tinkoff.simpleapp.common.db.{DatabaseModule, H2DatabaseModule}
import ru.tinkoff.simpleapp.notification.NotificationModule
import ru.tinkoff.simpleapp.payment.PaymentModule

import scala.annotation.unused


object PaymentApp extends App {
  private val runtimeModule: DefaultRuntimeModule = wire[DefaultRuntimeModule]
  import runtimeModule._

  @unused private val databaseModule: DatabaseModule = wire[H2DatabaseModule]
  @unused private val notificationModule: NotificationModule = wire[NotificationModule]
  @unused private val paymentModule: PaymentModule = wire[PaymentModule]
  @unused private val configModule: ConfigModule = wire[ConfigModule]
  private val app: AppModule = wire[AppModule]
  val logger = Logger[PaymentApp.type ]

  logger.debug("start application")


  for {
    _ <- app.initSchema()
    _ <- app.run()
  } yield ()

}
