package ru.tinkoff.simpleapp.notification

import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}

trait NotificationService {
  def sendNotification(text: String): Future[Unit]
}

class PushNotificationService extends NotificationService with LazyLogging {
  override def sendNotification(text: String): Future[Unit] = {
    logger.info(s"push with content '$text' sent")
    Future.unit
  }
}

class EmailNotificationService extends NotificationService with LazyLogging {
  override def sendNotification(text: String): Future[Unit] = {
    logger.info(s"email with content '$text' sent")
    Future.unit
  }
}

class MultiChanelNotificationService(chanels: Seq[NotificationService])(implicit ec: ExecutionContext) extends NotificationService {
  override def sendNotification(text: String): Future[Unit] = {
    Future.traverse(chanels)(_.sendNotification(text))
      .map(_ => ())
  }
}