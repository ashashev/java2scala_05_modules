package ru.tinkoff.simpleapp.notification

import com.softwaremill.macwire.Module
import ru.tinkoff.simpleapp.common.RuntimeModule
import com.softwaremill.macwire._

import scala.annotation.unused

@Module
class NotificationModule(runtimeModule: RuntimeModule) {
  import runtimeModule._

  private val pushNotificationService: PushNotificationService = wire[PushNotificationService]
  private val emailNotificationService: EmailNotificationService = wire[EmailNotificationService]

  val multiChanelNotificationService: MultiChanelNotificationService = {
    @unused val channels = Seq(pushNotificationService, emailNotificationService)
    wire[MultiChanelNotificationService]
  }
}
