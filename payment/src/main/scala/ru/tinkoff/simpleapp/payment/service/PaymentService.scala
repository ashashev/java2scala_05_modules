package ru.tinkoff.simpleapp.payment.service

import java.time.LocalDate
import java.util.UUID

import ru.tinkoff.simpleapp.notification.NotificationService
import ru.tinkoff.simpleapp.payment.dao.PaymentRepository

import scala.concurrent.{ExecutionContext, Future}


trait PaymentService {
  def find(paymentId: UUID): Future[Option[Payment]]

  def list(): Future[Seq[Payment]]

  def pay(sum: BigDecimal,
          sourceAccountNumber: String,
          targetAccountNumber: String,
          comment: String,
          date: LocalDate): Future[Payment]
}


class PaymentServiceImpl(paymentRepository: PaymentRepository,
                         notificationService: NotificationService)
                        (implicit ec: ExecutionContext) extends PaymentService {
  override def pay(sum: BigDecimal,
                   sourceAccountNumber: String,
                   targetAccountNumber: String,
                   comment: String,
                   date: LocalDate): Future[Payment] = {
    val payment = Payment(
      UUID.randomUUID(),
      sum,
      sourceAccountNumber,
      targetAccountNumber,
      comment,
      date,
      PaymentStatus.Submitted
    )

    for {
      _ <- paymentRepository.create(payment)
      _ <- notificationService.sendNotification(s"payment with sum: $sum submitted")
    } yield payment
  }

  override def find(paymentId: UUID): Future[Option[Payment]] = {
    paymentRepository.findPayment(paymentId)
  }

  override def list(): Future[Seq[Payment]] = {
    paymentRepository.list()
  }
}