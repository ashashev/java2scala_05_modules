package ru.tinkoff.simpleapp.payment.dao

import java.time.LocalDate
import java.util.UUID

import enumeratum.SlickEnumSupport
import ru.tinkoff.simpleapp.common.db.DB
import ru.tinkoff.simpleapp.payment.service.{Payment, PaymentStatus}
import slick.ast.BaseTypedType
import slick.lifted.ProvenShape


case class PaymentsTable(db: DB) extends SlickEnumSupport {
  import db.profile.api._

  override val profile = db.profile
  implicit val paymentStatusMapper: BaseTypedType[PaymentStatus] = mappedColumnTypeForEnum(PaymentStatus)

  class PaymentsTable(tag: Tag) extends Table[Payment](tag, "payments") {

    def id: Rep[UUID] = column("id")

    def sum: Rep[BigDecimal] = column("sum")

    def sourceAccountNumber: Rep[String] = column[String]("source_account_number")

    def targetAccountNumber: Rep[String] = column[String]("target_account_number")

    def date: Rep[LocalDate] = column[LocalDate]("date")

    def comment: Rep[String] = column[String]("comment")

    def status: Rep[PaymentStatus] = column[PaymentStatus]("status")

    override def * : ProvenShape[Payment] =
      (id, sum, sourceAccountNumber, targetAccountNumber, comment, date, status) <> (Payment.tupled, Payment.unapply)
  }

  val AllPayments = TableQuery[PaymentsTable]
}
