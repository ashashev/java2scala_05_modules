package ru.tinkoff.simpleapp.payment.controller

import java.time.LocalDate
import java.util.UUID

import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import ru.tinkoff.simpleapp.common.api.Controller
import ru.tinkoff.simpleapp.payment.service.PaymentService

case class CreatePaymentRequest(sum: BigDecimal,
                                sourceAccountNumber: String,
                                targetAccountNumber: String,
                                comment: String,
                                date: LocalDate)



class PaymentController(paymentService: PaymentService) extends Controller {

  lazy val route: Route =
    pathPrefix("api" / "v1" / "payment") {
      pathEnd {
        post {
          createPayment
        } ~
          get {
            listPayment
          }
      } ~ path(JavaUUID) { paymentId =>
        findPayment(paymentId)
      }
    }

  private val createPayment: Route =
    entity(as[CreatePaymentRequest]) { request =>
      import request._

      complete(paymentService.pay(sum, sourceAccountNumber, targetAccountNumber, comment, date))
    }

  private val listPayment: Route =
    complete(paymentService.list())

  def findPayment(paymentId: UUID): Route = complete {
    paymentService.find(paymentId)
  }
}
