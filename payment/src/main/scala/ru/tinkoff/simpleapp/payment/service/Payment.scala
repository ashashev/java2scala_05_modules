package ru.tinkoff.simpleapp.payment.service

import java.time.LocalDate
import java.util.UUID

import enumeratum.EnumEntry.UpperSnakecase
import enumeratum.{CirceEnum, Enum, EnumEntry}

case class Payment(id: UUID,
                   sum: BigDecimal,
                   sourceAccountNumber: String,
                   targetAccountNumber: String,
                   comment: String,
                   date: LocalDate,
                   status: PaymentStatus)

sealed trait PaymentStatus extends EnumEntry with UpperSnakecase

object PaymentStatus extends Enum[PaymentStatus] with CirceEnum[PaymentStatus] {
  case object Submitted extends PaymentStatus
  case object Declined extends PaymentStatus
  case object Completed extends PaymentStatus

  override def values: IndexedSeq[PaymentStatus] = findValues
}
