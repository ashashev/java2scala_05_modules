import sbt._
import Versions._


object Dependencies {
  val akkaHttp = Seq(
    "com.typesafe.akka" %% "akka-http" % "10.1.11",
    "de.heikoseeberger" %% "akka-http-circe" % "1.31.0"
  )

  val circe = Seq(
    "io.circe" %% "circe-core" % circeVersion,
    "io.circe" %% "circe-generic" % circeVersion,
    "io.circe" %% "circe-parser" % circeVersion,
  )

  val macwire = Seq(
    "com.softwaremill.macwire" %% "macros" % "2.3.3" % Provided,
    "com.softwaremill.macwire" %% "util" % "2.3.3"
  )


  val logging = Seq(
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
  )

  val slick = Seq(
    "com.typesafe.slick" %% "slick" % "3.3.2",
    "com.typesafe.slick" %% "slick-hikaricp" % "3.3.2",
    "com.h2database" % "h2" % "1.4.199",
  )

  val enumeratum = Seq(
    "com.beachape" %% "enumeratum" % "1.5.15",
    "com.beachape" %% "enumeratum-slick" % "1.5.16",
    "com.beachape" %% "enumeratum-circe" % "1.5.23",

  )

  val commonDependencies = akkaHttp ++ circe ++ macwire ++ logging ++ slick ++ enumeratum
}

object Versions {
  val circeVersion = "0.12.3"

}