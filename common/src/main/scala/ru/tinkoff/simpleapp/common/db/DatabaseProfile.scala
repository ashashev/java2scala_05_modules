package ru.tinkoff.simpleapp.common.db

import slick.jdbc.{H2Profile, JdbcProfile}
import com.softwaremill.macwire._

import scala.concurrent.Future


abstract class DB(val profile: JdbcProfile) {
  import profile.api._

  val db = Database.forConfig("database")

  def run[R, E <: Effect](action: IO[R, E]): Future[R] =
    db.run(action)
}


class H2Database extends DB(H2Profile)


@Module
trait DatabaseModule {
  val db: DB
}

class H2DatabaseModule extends DatabaseModule {
  override val db: DB = wire[H2Database]
}