package ru.tinkoff.simpleapp.common.api

trait ApiModule {
  def controllers: Seq[Controller]
}
