package ru.tinkoff.simpleapp.common

import akka.actor.ActorSystem

import scala.concurrent.ExecutionContext

trait RuntimeModule {
  implicit def actorSystem: ActorSystem
  implicit def executionContext: ExecutionContext
}

class DefaultRuntimeModule extends RuntimeModule {
  implicit override val actorSystem: ActorSystem = ActorSystem()
  implicit override val executionContext: ExecutionContext = actorSystem.dispatcher
}
