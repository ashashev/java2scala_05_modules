name := "05-payment-app"

version := "0.1"

lazy val common = module("common")

lazy val app = module("app")
  .dependsOn(payment, common)

lazy val payment = module("payment")
  .dependsOn(common, notifications)

lazy val notifications = module("notifications")
  .dependsOn(common)

def module(moduleName: String) =
  Project(moduleName, file(moduleName))
    .settings(
      scalaVersion := "2.13.1",
      scalacOptions := ScalacOptions.options,
      libraryDependencies ++= Dependencies.commonDependencies
    )